# New Mac Setup

## Manual Things

### Firefox

- Install Firefox: [http://firefox.com](http://firefox.com)
- Set Firefox as the default browser
- Set Density to Compact
- Set Theme to Light
- Set Default Search Engine to DuckDuckGo
- Uncheck everything in New Tab Preferences
- Uncheck “Remember logins and passwords for websites” under Privacy & Security (1Password will be in charge of all passwords)
- Check “Block new requests asking to allow notifications” under Notification Permissions
- Remove redundant icons from the toolbar (reload, flexible spaces etc.)
- Install Shut Up extension: [https://addons.mozilla.org/en-US/firefox/addon/shut-up-comment-blocker/](https://addons.mozilla.org/en-US/firefox/addon/shut-up-comment-blocker/)
- Install Pinboard extension: [https://addons.mozilla.org/en-US/firefox/addon/pinboardin/](https://addons.mozilla.org/en-US/firefox/addon/pinboardin/)
- Install AdBlock extension: [https://getadblock.com/firefox/](https://getadblock.com/firefox/)
- Install DuckDuckGo Privacy Essentials extension: [https://addons.mozilla.org/en-US/firefox/addon/duckduckgo-for-firefox/](https://addons.mozilla.org/en-US/firefox/addon/duckduckgo-for-firefox/)
- Add user styles from [https://github.com/sorig/firefox-mental-health-improver](https://github.com/sorig/firefox-mental-health-improver)

### Homebrew

- Install Homebrew: [https://brew.sh/](https://brew.sh/)
- Install Node.js: `brew install node`
- Install (update) Bash: `brew install bash`

### Dropbox

Install Dropbox: [https://www.dropbox.com/install](https://www.dropbox.com/install)

### 1Password

Install 1Password: [https://1password.com/downloads/](https://1password.com/downloads/)

### Visual Studio Code

- Install Visual Studio Code: [https://code.visualstudio.com/Download](https://code.visualstudio.com/Download)
- Download settings from personal Dotfiles repo: [https://gitlab.com/krnr/dotfiles](https://gitlab.com/krnr/dotfiles)
- Install [Legible Starry Night](https://marketplace.visualstudio.com/items?itemName=zzandy.legible-starry-night) theme
- Install Prettier
- Install `editor.fontFamily` font, if missing

### Quick Look plugins

- Install plugins from [https://github.com/sindresorhus/quick-look-plugins](https://github.com/sindresorhus/quick-look-plugins)

### Alfred

- Install Alfred: [https://alfredapp.com/](https://alfredapp.com/)
- Disable keyboard shortcut for Spotlight in System Preferences
- Set Alfred hotkey to “⌘ Space”
- Enable “Hide menu bar icon” in preferences under Appearance → Options
- Set DuckDuckGo as only fallback result under Default Results

### Gapplin

- Install Gapplin SVG Viewer: [http://gapplin.wolfrosch.com/](http://gapplin.wolfrosch.com/)

### Spectacle

- Install Spectacle: [https://www.spectacleapp.com/](https://www.spectacleapp.com/)
- Set Spectacle to run as a background application, rather than in the status menu
- Set Spectacle to Launch at login

### f.lux

- Install f.lux: [https://justgetflux.com/](https://justgetflux.com/)

### ProtonMail

- Install and configure Bridge: [https://protonmail.com/bridge/install](https://protonmail.com/bridge/install)

### Signal

- Install Signal: [https://signal.org/download/](https://signal.org/download/)

### System Preferences

- Desktop
  - Custom Color → `#1E1E1E`

### Terminal

- Add .bash_profile from personal Dotfiles repo: [https://gitlab.com/krnr/dotfiles](https://gitlab.com/krnr/dotfiles)
- Add chalk.terminal as default profile, from personal Dotfiles repo: [https://gitlab.com/krnr/dotfiles](https://gitlab.com/krnr/dotfiles)

## Command Line Things

**TODO:** Make all of these into a bash script that lets you run them all at once.

_Note:_ An overview of configurable things can be found via the `defaults read` command.

### Appearance

Set Graphite appearance:

    defaults write NSGlobalDomain AppleAquaColorVariant -int 6

Set highlight color to Yellow:

    defaults write NSGlobalDomain AppleHighlightColor -string "1.000 0.937 0.690"

Enable dark menu bar and Dock:

    defaults write NSGlobalDomain AppleInterfaceStyle -string "Dark"

Always show scrollbars:

    defaults write NSGlobalDomain AppleShowScrollBars -string "Always"

### Finder

Allow quitting Finder via ⌘ + Q; doing so will also hide desktop icons:

    defaults write com.apple.finder QuitMenuItem -bool true

Always hide the Desktop:

    defaults write com.apple.finder CreateDesktop false; killall Finder

Disable Finder window animations and Get Info animations:

    defaults write com.apple.finder DisableAllAnimations -bool true

Set Desktop as the default location for new Finder windows; for other paths, use `PfLo` and `file:///full/path/here/`:

    defaults write com.apple.finder NewWindowTarget -string "PfDe"
    defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/Desktop/

Show all filename extensions in Finder:

    defaults write NSGlobalDomain AppleShowAllExtensions -bool true

Show status bar in Finder:

    defaults write com.apple.finder ShowStatusBar -bool true

Show path bar in Finder:

    defaults write com.apple.finder ShowPathbar -bool true

Display full POSIX path as Finder window title:

    defaults write com.apple.finder \_FXShowPosixPathInTitle -bool true

When performing a search, search the current folder by default:

    defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

Disable the warning when changing a file extension:

    defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

Avoid creating .DS_Store files on network or USB volumes:

    defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
    defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

Enable snap-to-grid:

    /usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
    /usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist

Use column view in all Finder windows by default:

    defaults write com.apple.finder FXPreferredViewStyle -string "clmv"

Disable the warning before emptying the Trash:

    defaults write com.apple.finder WarnOnEmptyTrash -bool false

Enable AirDrop over Ethernet and on unsupported Macs running Lion:

    defaults write com.apple.NetworkBrowser BrowseAllInterfaces -bool true

Show the ~/Library folder:

    chflags nohidden ~/Library

Show the /Volumes folder:

    sudo chflags nohidden /Volumes

Expand the following File Info panes: “General”, “Open with”, and “Sharing & Permissions”:

    defaults write com.apple.finder FXInfoPanesExpanded -dict \
        General -bool true \
        OpenWith -bool true \
        Privileges -bool true

### Dock, Dashboard, and Hot Corners

Set an Ultra-Fast Dock Hide & Display Animation:

    defaults write com.apple.dock autohide-time-modifier -float 0.12; killall Dock

Set the icon size of Dock items to 36 pixels:

    defaults write com.apple.dock tilesize -int 36

Set minimize/maximize window effect to “Scale”:

    defaults write com.apple.dock mineffect -string "scale"

Wipe all (default) app icons from the Dock:

    defaults write com.apple.dock persistent-apps -array

Don’t animate opening applications from the Dock:

    defaults write com.apple.dock launchanim -bool false

Speed up Mission Control animations:

    defaults write com.apple.dock expose-animation-duration -float 0.1

Don’t group windows by application in Mission Control (i.e. use the old Exposé behavior instead)

    defaults write com.apple.dock expose-group-by-app -bool false

Speed up Mission Control animations:

    defaults write com.apple.dock expose-animation-duration -float 0.1

Disable Dashboard:

    defaults write com.apple.dashboard mcx-disabled -bool true

Remove the auto-hiding Dock delay:

    defaults write com.apple.dock autohide-delay -float 0

Remove the animation when hiding/showing the Dock:

    defaults write com.apple.dock autohide-time-modifier -float 0

Automatically hide and show the Dock:

    defaults write com.apple.dock autohide -bool true

Make Dock icons of hidden applications translucent:

    defaults write com.apple.dock showhidden -bool true

Disable the Launchpad gesture (pinch with thumb and three fingers):

    defaults write com.apple.dock showLaunchpadGestureEnabled -int 0

Setup Hot Corners:

    # Bottom right screen corner → Mission Control
    defaults write com.apple.dock wvous-br-corner -int 2
    defaults write com.apple.dock wvous-br-modifier -int 0

### Keyboard & Trackpad

Enable tap to click for this user and for the login screen:

    defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
    defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
    defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

Disable automatic capitalization as it’s annoying when typing code:

    defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

Disable smart dashes as they’re annoying when typing code:

    defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

Disable automatic period substitution as it’s annoying when typing code:

    defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false

Disable smart quotes as they’re annoying when typing code:

    defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

Disable auto-correct:

    defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

Disable the character chooser popup, and get proper key repeat:

    defaults write -g ApplePressAndHoldEnabled -bool false

Use scroll gesture with the Ctrl (^) modifier key to zoom, and disable pixel smoothing:

    defaults write com.apple.universalaccess closeViewScrollWheelToggle -bool true
    defaults write com.apple.universalaccess closeViewSmoothImages -bool false
    defaults write com.apple.universalaccess HIDScrollZoomModifierMask -int 262144

Follow the keyboard focus while zoomed in:

    defaults write com.apple.universalaccess closeViewZoomFollowsFocus -bool true

Set a blazingly fast keyboard repeat rate:

    defaults write NSGlobalDomain KeyRepeat -int 1
    defaults write NSGlobalDomain InitialKeyRepeat -int 10

### Misc.

Set computer name to “krnr-mbp” (as done via System Preferences → Sharing):

    sudo scutil --set ComputerName "krnr-mbp"
    sudo scutil --set HostName "krnr-mbp"
    sudo scutil --set LocalHostName "krnr-mbp"
    sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string "krnr-mbp"

Make terminal logins fast:

    touch ~/.hushlogin

Disable save to iCloud, and instead save to disk by default:

    defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

Disable Notification Center (might require you to disable System Integrity Protection – remember to re-enable it!):

    launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist

Show (sound) volume in menu bar:

    defaults write com.apple.systemuiserver com.apple.menuextra.volume -int 1

Show date in menu bar:

    defaults write com.apple.menuextra.clock DateFormat -string "EEE d MMM HH.mm"

Enable automatic update check:

    defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true

Prevent Photos from opening automatically when devices are plugged in:

    defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true

Expand save panel by default:

    defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
    defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

Expand print panel by default:

    defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
    defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

Automatically quit printer app once the print jobs complete:

    defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true

Disable the “Are you sure you want to open this application?” dialog:

    defaults write com.apple.LaunchServices LSQuarantine -bool false

Restart automatically if the computer freezes:

    sudo systemsetup -setrestartfreeze on
