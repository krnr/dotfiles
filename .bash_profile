export PATH="/usr/local/bin:$PATH"
export EDITOR=nano

HISTIGNORE="$HISTIGNORE:jrnl *"

STARTCOLOR='\[\e[0;33m\]';
ENDCOLOR="\[\e[0m\]";
PEACE="\[🕊\]";
export PS1="$STARTCOLOR\W$ENDCOLOR $PEACE  "

# Don't save duplicates in history
export HISTCONTROL=ignoreboth:erasedups

# Open Desktop directory
alias desk='cd ~/Desktop'

# Open Development directory
alias code='cd ~/code'

# Play 'Hero' sound
alias sound='afplay /System/Library/Sounds/Glass.aiff'

# mkdir and go there
mkcd() {
  mkdir -p $1 && cd $1
}

# Show/hide hidden files + kill Finder
show() {
  defaults write com.apple.finder AppleShowAllFiles TRUE; killAll Finder
  echo 'Viser nu skjulte filer.'
}
hide() {
  defaults write com.apple.finder AppleShowAllFiles FALSE; killAll Finder
  echo 'Skjuler nu skjulte filer.'
}

# List files in all kinds of ways
alias ls='ls -1 -a'
alias ll='ls -alh'
alias la='ls -A'
alias l='ls -CFlh'
alias lsd="ls -alF | grep /$"

# cd cd cd cd
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# Open directory in Visual Studio Code
vsc() {
  open $1 -a "Visual Studio Code"
}

# Create audiobook from text
dumbog() {
  tmp=$(pbpaste | textutil -convert txt -stdin -stdout)
  say -v Samantha -f $tmp -o "$1.m4a"
}

# Path to notes directory (alias)
NOTESPATH='~/Dropbox\ \(Personal\)/notes/';

# Go to notes directory
alias notes="cd $NOTESPATH"

# Create FoldingText note and open it
ft() {
  touch "$1.ft" && open -a "FoldingText" "$1.ft"
}

# Count characters in string
charcount() {
  echo "`echo -n $@ | wc -m | xargs`"
}

titlecase() {
  echo $@ | titlecaser
}

# Pretty git log (from Esben)
gitlog() {
  git log --graph --pretty=oneline --abbrev-commit
}

# Reset MAC address to a random value
alias freewifi="sudo ifconfig en0 ether `openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'`"

# Generate Duckwise datestamp for filenames based on current date
datestamp() {
  stamp=$(date +%Y%m%d)
  echo "`echo $stamp was copied to the clipboard! 🦆`"
  echo -n $stamp | pbcopy
}

# Get synonyms for words from wordnet
synonym() {
  wn $1 -syns{n,v,a,r}
}

# Output .gitignore preset for Node.js and macOS
gitignoreplz() {
  curl https://www.gitignore.io/api/node,macos
}
